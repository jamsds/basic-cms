Rails.application.routes.draw do
  root to: "front#index"

  # About
  get '/about', to: "front#about"
  get '/price', to: "front#price"

  # Search
  get '/search', to: "front#search"

  # Contact
  get '/contact', to: "front#contact"
  post '/contact/send' => "front#contactSubmit"

  # Upload
  post "/upload_image" => "upload#upload_image"
  post '/delete_image' => 'upload#delete_image'
  get '/load_images'   => 'upload#load_images', :defaults => { :format => 'json' }
  
  # Back-End
  namespace :acp do
    get '/', to: "main#index"

    resources :posts

    resources :categories do
      resources :subcategories
    end

    resources :pricings

    get '/setting', to: "setting#index"
    get '/setting/cache', to: "setting#cache"
    post '/setting/cache/clean', to: "setting#clean"
    match '/setting/update', to: "setting#update", via: [:post, :patch]
  end

  # Authenticator Admin
  devise_for :admins, controllers: {
    sessions: "admins/sessions",
    registrations: "admins/registrations",
    passwords: "admins/passwords"
  }

  # Post
  get '/:category', to: "posts#index"
  get '/:category/:subcategory', to: "posts#category"
  get '/:category/:subcategory/:post', to: "posts#detail"

  match '*path' => 'front#errors', via: :all
end
