$.FroalaEditor.DefineIcon('quote', { NAME: 'fas fa-quote-left' });
$.FroalaEditor.RegisterCommand('quote', {
  title: 'Insert Quote',
  focus: true,
  undo: true,
  refreshAfterCallback: true,
  callback: function () {
    this.html.insert('<p class="quote"></p>');
  }
});