// jQuery 3.x
//= require jquery3

// Basic Rails
//= require rails-ujs
//= require activestorage
//= require turbolinks

// Bootstrap 4.x
//= require popper
//= require bootstrap

// MomentJs
// require libs/moment.min

// require libs/md5

// Editor
//= require libs/froala/codemirror.min
//= require libs/froala/xml.min
//= require libs/froala/froala_editor.pkgd.min
//= require libs/froala/froala_function

$(document).on('turbolinks:load', function() {
	// Truncate
	$(".truncate").css("-webkit-box-orient","vertical");

	function slug(title) {
	  var slug;

    slug = title.toLowerCase();

    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');

    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    
    slug = slug.replace(/ /gi, "-");

    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');

    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    
    return slug;
	}

	function editorFunction() {
		$('#title').keyup(function() {
      var title = $(this).val();
      $('#slug').val(slug(title));
      $('#title-slug').text(slug(title));
    });

		$('.wysiwyg').froalaEditor({
			heightMin: 150,
			imageUploadURL: '/upload_image',
			imageManagerLoadURL: '/load_images',
			imageManagerDeleteURL: '/delete_image',
			iconsTemplate: 'font_awesome_5',
			toolbarButtons: ['bold', 'italic', 'underline', '|', 'quote', '|', 'fontSize', 'align', 'color', '|', 'insertLink', 'insertImage', 'insertTable', '|', 'html', 'clearFormatting', '|', 'undo', 'redo'],
			tableStyles: {
				table : 'Bootstrap'
			},
			quickInsertButtons: ['image', 'table'],
			placeholderText: 'Nhập nội dung',
			wordPasteKeepFormatting: false
		});
	}

	editorFunction();

	$(".admin__control-container-resize").on('click', function() {
		$(".admin__control-navigator").toggleClass("colapse");
		$(".admin__control-container").toggleClass("expand");
	})

	$(".ribbon-show").click(function() {
    $(".ribbon").removeClass("focus")
    $(this).parent().addClass("focus");

    $(".btn-edit-post-modal").click(function() {
      $("body").addClass("no-scroll");
      $(".edit-post-modal").removeClass("hidden");

      $("#postModal").load('/acp/posts/'+$(this).attr('post-data')+'/edit?remote=true #form', function() {
        editorFunction();
        $(".loading").hide();
      });
    });
  })

  $(document).keyup(function(e) {
    if (e.keyCode === 27) {
      $("body").removeClass("no-scroll");

      $(".edit-post-modal").addClass("hidden");
      $("#postModal").empty();
      $(".loading").show();
    }
  });

  window.onclick = function(event) {
    if (!event.target.matches(".ribbon-show") && !event.target.matches(".ribbon-action-list") && !event.target.matches(".form-control")) {
      var ribbon = document.getElementsByClassName("ribbon");
      var i;
      for (i = 0; i < ribbon.length; i++) {
        var openRIBBON = ribbon[i];
        if (openRIBBON.classList.contains('focus')) {
          openRIBBON.classList.remove('focus');
        }
      }
    }
  }
});