// jQuery 3.x
// require jquery3

// Basic Rails
//= require rails-ujs
//= require activestorage
//= require turbolinks

// Bootstrap 4.x
// require popper
// require bootstrap

//= require libs/cookie.min
//= require libs/jquery.lazy.min

// Defind WindowsWidth
var windowsize = $(window).width();

var url = window.location.pathname;
var path = url.split( '/' );

function baseFunction() {
	// Lazyload
	$('img').lazy();

	// Truncate
  $(".truncate, .truncate-nd").css("-webkit-box-orient","vertical");
  
  // CSRF
  $("#csrf-token").val(document.querySelector("meta[name=csrf-token]").content);
  
  (function(){
    $(".mobile__nav-list--item").click(function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
      } else {
        $(".mobile__nav-list--item").removeClass("active");
        $(this).addClass("active");
      }
    })

    $(".mobile__toggle").click(function() {
      $(".mobile__nav").toggleClass("open");
      $("body").toggleClass("no-scroll");
    })

    $(".rectangle").each(function() {
      $(this).css("height", $(this).width() + 48);
      $(this).css("border-radius", $(this).height());
    })
  })();
}

function slug(title) {
  var slug;

  slug = title.toLowerCase();

  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');

  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  
  slug = slug.replace(/ /gi, "-");

  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');

  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  
  return slug;
}

document.addEventListener("turbolinks:load", function() {
  $(document).ready(function() {
    baseFunction();
  });
});


var stage = 0;
document.addEventListener("scroll", function() {
  var current = $(window).scrollTop();
  current++;

  if (document.getElementById("related")) {
    scroll = current + document.getElementById("related").offsetHeight + document.getElementById("footer").offsetHeight + document.getElementById("hero").offsetHeight + 2
    footer = document.getElementById("footer").offsetTop

    if (current > document.getElementById("hero").offsetTop + document.getElementById("hero").offsetHeight) {
      document.getElementById("related").style.width = document.getElementById("related").offsetWidth + "px";
      document.getElementById("related").classList.add("related-fixed");
    } else {
      document.getElementById("related").classList.remove("related-fixed");
    }

    if (scroll > footer) {
      document.getElementById("related").classList.remove("related-fixed");
    }
  }

  // Scroll up & down
  // if (current > stage) {
  //   console.log("up")
  // } else {
  //   console.log("down")
  // }
  // stage = current;
});