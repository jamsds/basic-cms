class NotificationMailer < ApplicationMailer
  default from: 'no-reply@vnetwork.vn'
  layout 'mailer'

  def contact_email(email, name, phone, subject, message)
  	@email, @name, @phone, @subject, @message = email, name, phone, subject, message

    @receiver = Setting.first.present? ? Setting.first.email : 'tho.nguyen@vnetwork.vn'

  	mail(
      to: @receiver,
      content_type: "text/html",
      subject: "Thông Báo Có Liên Hệ Từ Website",
    )
  end
end