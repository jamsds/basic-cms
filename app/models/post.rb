class Post < ApplicationRecord
	after_save :clear_cache
	after_destroy :clear_cache

	has_attached_file :thumb,
	{
		styles: {
			full:  ["1920x1080#",:jpg],
			thumb:  ["366x250#",:jpg]
		},
		default_url: ":attachment/null",
		s3_permissions: :private,
	  path: "/uploads/:attachment/:style/:filename",
	  validate_media_type: false
	}
	validates_attachment_file_name :thumb, :matches => [/png\Z/, /jpe?g\Z/, /svg\Z/]

	private
	  def clear_cache
	    $redis.del "posts"
	  end
end