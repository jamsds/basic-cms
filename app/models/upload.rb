class Upload < ApplicationRecord
	has_attached_file :file, { :default_url => ":attachment/null", s3_permissions: :private, :path => "/:class/:attachment/:filename" }
	do_not_validate_attachment_file_type :file
end