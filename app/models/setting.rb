class Setting < ApplicationRecord
	after_save :clear_cache
	after_destroy :clear_cache

	private
	  def clear_cache
	    $redis.del "settings"
	  end
end