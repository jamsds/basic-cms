class UploadController < ApplicationController
  # Disable CSRF for upload
	skip_before_action :verify_authenticity_token, only: [:upload_image, :load_images, :delete_image]

	def upload_image
    if params[:file]
      Upload.create(file: params[:file])
      @file = Upload.last.file.url
      render :json => {:link => @file}.to_json
    else
      render :text => {:link => nil}.to_json
    end
  end

  def load_images
    @files = Upload.all
  end

  def delete_image
    Upload.find(params[:id]).destroy
  end
end