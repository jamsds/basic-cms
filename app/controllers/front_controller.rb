class FrontController < ApplicationController
	skip_before_action :verify_authenticity_token, only: [:contactSubmit]

	def index
		@posts = @posts.select { |i| i["category"] == 1 }
	end

	def setting
		@setting = Setting.new(about: params["setting"]["about"])
	end

	def search
		@categories = fetch_categories
		@subcategories = fetch_subcategories
		@posts = fetch_posts

		if params[:s].present?
			@results = @posts.select { |i| i["slug"].to_s.include? slug(params[:s]) }
		else
			@results = []
		end
	end

	def contactSubmit
		NotificationMailer.contact_email(
			params["email"],
			params["name"],
			params["phone"],
			params["address"],
			params["subject"]
		).deliver

		flash[:contact_success] = "Successfull. Contact has been sent"

		redirect_back(fallback_location: root_path)
	end

	def errors
		@requested_path = request.path
		flash[:path_notice] = @requested_path
	end

	private
		def slug(string)
			string.downcase.gsub(/[.,]/, '').gsub(' ','-').gsub(/[áàảạãăắằẳẵặâấầẩẫậ]/, 'a').gsub(/[éèẻẽẹêếềểễệ]/, 'e').gsub(/[iíìỉĩị]/, 'i').gsub(/[óòỏõọôốồổỗộơớờởỡợ]/, 'o').gsub(/[úùủũụưứừửữự]/, 'u').gsub(/[ýỳỷỹỵ]/, 'y').gsub(/[đ]/, 'd')
		end
end