class Acp::SubcategoriesController < AcpController
	before_action :find_subcategory, only: [:show, :edit, :update, :destroy]

	def index
		@category = Category.find(params[:category_id])
		redirect_to acp_category_path(@category)
	end

	def new
		@category = Category.find(params[:category_id])
		@subcategory = @category.subcategories.new
	end

	def create
		@category = Category.find(params[:category_id])
		@subcategory = @category.subcategories.new(subcategory_params)

		if @subcategory.save
			Subcategory.last.update(thumb_url: Subcategory.last.thumb.url(:full))
			redirect_to acp_category_path(@category)
		end
	end

	def edit
	end

	def update
		if @subcategory.update(subcategory_params)
			@subcategory.update(thumb_url: @subcategory.thumb.url(:full))
      redirect_to acp_category_path(@category)
    else
      render 'edit'
    end
	end

	def destroy
		if @subcategory.destroy
      redirect_to acp_category_path(@category)
    end
	end

	private
    def subcategory_params
    	params.require(:subcategory).permit(:title, :slug, :description, :detail, :thumb, :featured, :hidden)
    end

    def find_subcategory
    	@category = Category.find(params[:category_id])
    	@subcategory = @category.subcategories.find(params[:id])
    end
end