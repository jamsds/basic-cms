class Acp::CategoriesController < AcpController
	before_action :find_category, only: [:show, :edit, :update, :destroy]

	def index
		@categories = Category.where(category_type: params[:type].present? ? params[:type] : [1,2,3]).order('category_type ASC','id ASC')
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)

		if @category.save
			Category.last.update(thumb_url: Category.last.thumb.url(:thumb))
			redirect_to acp_categories_path
		end
	end

	def edit
	end

	def update
		if @category.update(category_params)
			@category.update(thumb_url: @category.thumb.url(:thumb))
      redirect_to acp_categories_path
    else
      render 'edit'
    end
	end

	def destroy
		if @category.destroy
      redirect_to acp_categories_path
    end
	end

	private
    def category_params
    	params.require(:category).permit(:title, :slug, :description, :detail, :category_type, :thumb, :featured, :hidden)
    end

    def find_category
    	@category = Category.find(params[:id])
    end
end