class Acp::PricingsController < AcpController
  before_action :find_post, only: [:show, :edit, :update, :destroy]

	def index
		@pricings = Pricing.all.order("created_at ASC")
	end

	def new
		@pricing = Pricing.new
	end

	def create
		@pricing = Pricing.new(pricing_params)

		if @pricing.save
			@pricing.update(thumb_url: @pricing.thumb.url)
			redirect_to acp_pricings_path
		end
	end

	def edit
	end

	def update
		if @pricing.update(pricing_params)
			@pricing.update(thumb_url: @pricing.thumb.url)
      redirect_to acp_pricings_path
    else
      render 'edit'
    end
	end

	def destroy
		if @pricing.destroy
      redirect_to acp_pricings_path
    end
	end

	private
    def pricing_params
    	params.require(:pricing).permit(:title, :slug, :description, :pricing, :thumb, :hidden)
    end

    def find_post
    	@pricing = Pricing.find(params[:id])
    end
end