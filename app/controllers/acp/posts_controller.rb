class Acp::PostsController < AcpController
	before_action :find_post, only: [:show, :edit, :update, :destroy]

	def index
		@posts = Post.all
	end

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(post_params)

		if @post.save
			Post.last.update(thumb_url: Post.last.thumb.url(:full))
			redirect_to acp_posts_path
		end
	end

	def edit
	end

	def update
		if @post.update(post_params)
			@post.update(thumb_url: @post.thumb.url(:full))
      redirect_to acp_posts_path
    else
      render 'edit'
    end
	end

	def destroy
		if @post.destroy
      redirect_to acp_posts_path
    end
	end

	private
    def post_params
    	params.require(:post).permit(:title, :slug, :description, :category, :content, :thumb, :featured, :hidden)
    end

    def find_post
    	@post = Post.find(params[:id])
    end
end
