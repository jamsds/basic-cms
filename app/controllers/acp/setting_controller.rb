class Acp::SettingController < AcpController
	def index
		@setting = Setting.first.nil? ? Setting.new : Setting.first
	end

	def update
		@setting = Setting.first.nil? ? Setting.new(setting_params) : Setting.first.update(setting_params)

		if Setting.first.nil?
			@setting.save
		end

		redirect_to acp_setting_path
	end

	def clean
		$redis.del "categories"
		$redis.del "subcategories"
		$redis.del "posts"
		$redis.del "settings"

		redirect_back(fallback_location: acp_path)
	end

	private
		def setting_params
			params.require(:setting).permit(:title, :description, :about, :phone, :email, :address, :company, :g_verification, :g_analytic, :fb_app, :fb_pixel)
		end
end