class ApplicationController < ActionController::Base
  before_action :settings
  before_action :set_categories

  protect_from_forgery with: :exception

  private
    def settings
      @settings = fetch_settings
    end

    def set_categories
      @categories = fetch_categories.select { |i| i["hidden"] == false }
      @subcategories = fetch_subcategories.select { |i| i["hidden"] == false }
      @posts = fetch_posts.select { |i| i["hidden"] == false }
      @pricings = Pricing.all.order("created_at ASC").select { |i| i["hidden"] == false }
    end

    def fetch_settings
      begin
        settings = $redis.get "settings"
        if settings.nil?
          settings = Setting.all.to_json
          $redis.set "settings", settings
        end
        settings = JSON.load settings
      rescue => error
        puts error.inspect
        settings = Setting.all
      end
      settings
    end

    def fetch_categories
      begin
        categories = $redis.get "categories"
        if categories.nil?
          categories = Category.all.to_json
          $redis.set "categories", categories
        end
        categories = JSON.load categories
      rescue => error
        puts error.inspect
        categories = Category.all
      end
      categories
    end

    def fetch_subcategories
      begin
        subcategories = $redis.get "subcategories"
        if subcategories.nil?
          subcategories = Subcategory.all.to_json
          $redis.set "subcategories", subcategories
        end
        subcategories = JSON.load subcategories
      rescue => error
        puts error.inspect
        subcategories = Subcategory.all
      end
      subcategories
    end

    def fetch_posts
      begin
        posts = $redis.get "posts"
        if posts.nil?
          posts = Post.all.to_json
          $redis.set "posts", posts
        end
        posts = JSON.load posts
      rescue => error
        puts error.inspect
        posts = Post.all
      end
      posts
    end
end