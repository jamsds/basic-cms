class PostsController < ApplicationController
  def index
    if @categories.find { |i| i["slug"] == params[:category] }.present?
      @category = @categories.find { |i| i["slug"] == params[:category] }
    else
      return redirect_to root_path
    end

    subcategory_ids = []
    @subcategories.select { |i| subcategory_ids << i["id"] if (i["category_id"] == @category["id"].to_i) }
    @posts = @posts.select { |i| subcategory_ids.include? i["category"] }
  end

  def category
    if @subcategories.find { |i| i["slug"] == params[:subcategory] }.present?
      @subcategory = @subcategories.find { |i| i["slug"] == params[:subcategory] }
      @source = @subcategory
    else
      return redirect_to root_path
    end

    @category = @categories.find { |i| i["id"] == @subcategory["category_id"].to_i }
    
    if @category["category_type"] == 1
      @posts = @posts.select { |i| i["category"] == @subcategory["id"].to_i }
    end

    @related = @posts.select { |i| i["category"] == 1 }.last(5)
  end

  def detail
    @post = @posts.find { |i| i["slug"] == params[:post] }
    @source = @post

    if @post.present?
      @related = @posts.select { |i| i["category"] == @post["category"].to_i && i["id"] != @post["id"] }.last(5)
    else
      return redirect_to root_path
    end
  end
end