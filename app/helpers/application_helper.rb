module ApplicationHelper
	def boolean(boolean)
    case boolean
      when true
        'có'
      when false
        'không'
    end
  end

  def category(id)
    case id
      when 1
        t 'category.post'
      when 2
        t 'category.product'
      when 3
      	t 'category.service'
    end
  end

  def slug(string)
		string.downcase.gsub(/[.,]/, '').gsub(' ','-').gsub(/[áàảạãăắằẳẵặâấầẩẫậ]/, 'a').gsub(/[éèẻẽẹêếềểễệ]/, 'e').gsub(/[iíìỉĩị]/, 'i').gsub(/[óòỏõọôốồổỗộơớờởỡợ]/, 'o').gsub(/[úùủũụưứừửữự]/, 'u').gsub(/[ýỳỷỹỵ]/, 'y').gsub(/[đ]/, 'd')
	end

  def mobile_device?
    if cookies[:mobile_param]
      cookies[:mobile_param] == "1"
    else
      request.user_agent =~ /Mobile|webOS/
    end
  end
end
