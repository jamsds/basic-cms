class CreatePricings < ActiveRecord::Migration[5.2]
  def self.up
    create_table :pricings do |t|
      t.string   :title
      t.string   :description
      t.string	 :slug
      t.string	 :pricing

      t.string   :thumb_url
    	t.attachment  :thumb

      t.boolean  :featured,   default: false
      t.boolean  :hidden,     default: false

      t.timestamps
    end
  end

  def self.down
    remove_attachment :posts, :thumb
  end
end
