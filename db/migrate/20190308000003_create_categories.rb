class CreateCategories < ActiveRecord::Migration[5.2]
  def self.up
    create_table :categories do |t|
      t.string   :title
      t.string   :description
      
    	t.string   :slug
      t.integer  :category_type
      
      t.text     :detail

      t.string   :thumb_url
      t.attachment  :thumb

      t.boolean  :featured,   default: false
      t.boolean  :hidden,     default: false
    end
  end

  def self.down
    remove_attachment :categories, :thumb
  end
end
