class CreatePosts < ActiveRecord::Migration[5.2]
  def self.up
    create_table :posts do |t|
      t.string   :title
      t.string   :description
      t.string	 :slug
      t.text     :content

      t.string   :thumb_url
    	t.attachment  :thumb

      t.integer  :category

      t.boolean  :featured,   default: false
      t.boolean  :hidden,     default: false

      t.timestamps
    end
  end

  def self.down
    remove_attachment :posts, :thumb
  end
end
