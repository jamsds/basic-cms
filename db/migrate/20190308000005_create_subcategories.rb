class CreateSubcategories < ActiveRecord::Migration[5.2]
  def self.up
    create_table :subcategories do |t|
      t.string   :title
      t.string   :description
      
  		t.references 	:category, foreign_key: true

      t.string   :slug
      
      t.text     :detail

      t.string   :thumb_url
      t.attachment  :thumb

      t.boolean  :featured,   default: false
      t.boolean  :hidden,     default: false
    end
  end

  def self.down
    remove_attachment :subcategories, :thumb
  end
end
