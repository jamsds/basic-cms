class CreateUploads < ActiveRecord::Migration[5.2]
  def self.up
    create_table :uploads do |t|
      t.attachment  :file
    end
  end

  def self.down
    remove_attachment :uploads, :file
  end
end
