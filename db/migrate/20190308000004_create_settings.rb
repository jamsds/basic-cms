class CreateSettings < ActiveRecord::Migration[5.2]
  def self.up
    create_table :settings do |t|
      # About
      t.string   :title
      t.string   :description
      t.string   :about
      t.string   :address
      t.string   :company

      # Contact Info
      t.string   :email
      t.string   :phone

      # Google Code
      t.string   :g_verification
      t.string   :g_analytic

      # Facebook Code
      t.string   :fb_app
      t.string   :fb_pixel

      # Schema
      t.string   :schema

      # Tawk.to Livechat SSID
      t.string   :tawk

      # Custom CSS & JS
      t.text     :custom_css
      t.text     :custom_js
    end
  end
end
