# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_30_022459) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "slug"
    t.integer "category_type"
    t.text "detail"
    t.string "thumb_url"
    t.string "thumb_file_name"
    t.string "thumb_content_type"
    t.bigint "thumb_file_size"
    t.datetime "thumb_updated_at"
    t.boolean "featured", default: false
    t.boolean "hidden", default: false
    t.integer "order_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "slug"
    t.text "content"
    t.string "thumb_url"
    t.string "thumb_file_name"
    t.string "thumb_content_type"
    t.bigint "thumb_file_size"
    t.datetime "thumb_updated_at"
    t.integer "category"
    t.boolean "featured", default: false
    t.boolean "hidden", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pricings", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "slug"
    t.string "pricing"
    t.string "thumb_url"
    t.string "thumb_file_name"
    t.string "thumb_content_type"
    t.bigint "thumb_file_size"
    t.datetime "thumb_updated_at"
    t.boolean "featured", default: false
    t.boolean "hidden", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "about"
    t.string "address"
    t.string "company"
    t.string "email"
    t.string "phone"
    t.string "g_verification"
    t.string "g_analytic"
    t.string "fb_app"
    t.string "fb_pixel"
    t.string "schema"
    t.string "tawk"
    t.text "custom_css"
    t.text "custom_js"
  end

  create_table "subcategories", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.bigint "category_id"
    t.string "slug"
    t.text "detail"
    t.string "thumb_url"
    t.string "thumb_file_name"
    t.string "thumb_content_type"
    t.bigint "thumb_file_size"
    t.datetime "thumb_updated_at"
    t.boolean "featured", default: false
    t.boolean "hidden", default: false
    t.integer "order_id"
    t.index ["category_id"], name: "index_subcategories_on_category_id"
  end

  create_table "uploads", force: :cascade do |t|
    t.string "file_file_name"
    t.string "file_content_type"
    t.bigint "file_file_size"
    t.datetime "file_updated_at"
  end

  add_foreign_key "subcategories", "categories"
end
